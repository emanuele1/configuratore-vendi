
/* apre e chiude contenitore articoli della sidebar  */

function myFunctionProdotti(myElement) {
    var contenitore = document.getElementById("contenitore-articoli");
    var freccia = myElement;

    if (contenitore.className == "open") {
        contenitore.className = "contenitore-articoli";
        freccia.classList.remove("open");
        freccia.classList.add("closed");
        console.log(freccia);
    } else {
        contenitore.className = "open";
        freccia.classList.remove("closed");
        freccia.classList.add("open");
        console.log(freccia);
    }
}

/* apre e chiude il contenitore per caricare file nella sidebar */

function uploadFiles(myElement) {
    var contenitore = document.getElementById("upload-files");
    var freccia = myElement;

    if (contenitore.className == "open") {
        contenitore.className = "contenitore-file";
        freccia.classList.remove("open");
        freccia.classList.add("closed");
        console.log(freccia);
    } else {
        contenitore.className = "open";
        freccia.classList.remove("closed");
        freccia.classList.add("open");
        console.log(freccia);
    }
}

/* funzione che chiude i vari contenitori della sidebar cliccando il contenitore centrale del prodotto in evidenza  */

function chiusuraUpload(){
    var uploadAperto = document.getElementById("upload-files");

    var contenitoreStickers = document.getElementById("contenitore-stickers");

    var contenitore = document.getElementById("contenitore-scroll");

    var freccia = document.getElementById("freccia-carica");
    var frecciaTesto = document.getElementById("freccia-testo");

    var frecciaStickers = document.getElementById("freccia-stickers");

    if(uploadAperto.className = "open"){
        uploadAperto.classList.remove("open");
        uploadAperto.className = "contenitore-file";
        freccia.classList.remove("open");
        freccia.classList.add("closed");
    }

    if(contenitore.className = "open"){
        contenitore.classList.remove("open");
        contenitore.className = "contenitore-scroll";
        frecciaTesto.classList.remove("open");
        frecciaTesto.classList.add("closed");
    }

    if(contenitoreStickers.className = "open"){
        contenitoreStickers.classList.remove("open");
        contenitoreStickers.className = "contenitore-scroll";
        frecciaStickers.classList.remove("open");
        frecciaStickers.classList.add("closed");
    }


}

/* apre e chiude la sezione delle magliette nella sidebar */

function aperturaSezioneMagliette(myElement2) {
    var contenitore = document.getElementById("contenitore-apribile");
    
    var freccia = myElement2;
    

    if (contenitore.className == "open") {
        
        
        contenitore.className = "sezione-apribile-shirt";
        freccia.classList.remove("open");
        freccia.classList.add("closed");
        console.log(freccia);
    } else {
        contenitore.className = "open";
        freccia.classList.remove("closed");
        freccia.classList.add("open");
        console.log(freccia);
    }
}

/* chiude la sezione delle magliette nella sidebar cliccando il contenitore centrale del prodotto in evidenza  */

function chiusuraSezioneMagliette(){
    
    var contenitoreAperto = document.getElementById("contenitore-apribile"); 

    if(contenitoreAperto.className = "open"){
        contenitoreAperto.classList.remove("open");
        contenitoreAperto.className = "sezione-apribile-shirt";
    }
}



/* apre la modale per le informazioni della maglietta singola */

function myFunction3() {
    var contenitore = document.getElementById("info-maglie");

    var stampa = document.getElementById("stampa");
    var imgProdotto = document.getElementById("div-img-prodotto");
    var art = document.getElementById("articoli");
    var black = document.getElementById("div-black");
    var contenitoreApribile = document.getElementById("contenitore-apribile");
    var testoColori = document.getElementById("testo-colori");
    

    if (contenitore.className == "open") {
        contenitore.className = "informazioni-maglie";
        stampa.className = "attivato";
        imgProdotto.className = "attivato"; 
        art.className = "attivato"; 
        black.className = "black";
        testoColori.className = "attivato";
        
    } else {
        contenitore.className = "open";
        stampa.className = "disattivato";
        imgProdotto.className = "disattivato";
        art.className = "disattivato";
        black.className = "mod";
        contenitoreApribile.className = "disattivato";
        testoColori.className = "disattivato";
    }
}


/* colorazione al click delle categorie per le t-shirt */

function colorazioneCategoria() {

    
    var categoria = document.getElementById("categorie-magliette-donna");
    


    if (categoria.className == "contenitore-uomo-donna-kids") {
        
        categoria.classList.remove("contenitore-uomo-donna-kids");
        categoria.classList.add("attiva");
       
        
    } else {
        categoria.className = "contenitore-uomo-donna-kids";
        
    }
}

/* decolorazione della categoria al click di un altra categoria */

function decolorazioneCategoriaDonna() {

    
    var categoria = document.getElementById("categorie-magliette-donna");
    


    if (categoria.className == "attiva") {
        
        categoria.classList.remove("attiva");
        categoria.classList.add("contenitore-uomo-donna-kids");
       
        
    } 
}

function decolorazioneCategoriaKids() {

    var categoriaKids = document.getElementById("categorie-magliette-kids");

    if(categoriaKids.className == "attiva"){
        categoriaKids.classList.remove("attiva");
        categoriaKids.classList.add("contenitore-uomo-donna-kids");
    }
}

function decolorazioneCategoriaUomo() {

    var categoriaUomo = document.getElementById("categorie-magliette-uomo");

    if(categoriaUomo.className == "contenitore-uomo-donna-kids-default"){
        categoriaUomo.classList.remove("contenitore-uomo-donna-kids-default");
        categoriaUomo.classList.add("contenitore-uomo-donna-kids");
    }
}


function colorazioneCategoria2() {

    
    var categoria = document.getElementById("categorie-magliette-kids");


    if (categoria.className == "contenitore-uomo-donna-kids") {
        
        categoria.classList.remove("contenitore-uomo-donna-kids");
        categoria.classList.add("attiva");
       
        
    } else {
        categoria.className = "contenitore-uomo-donna-kids";
        
    }
}

function colorazioneCategoria3() {

    
    var categoria = document.getElementById("categorie-magliette-uomo");


    if (categoria.className == "contenitore-uomo-donna-kids") {
        
        categoria.classList.remove("contenitore-uomo-donna-kids");
        categoria.classList.add("contenitore-uomo-donna-kids-default");
       
        
    } 
}


/* colorazione al click icone allineamento della sezione testo*/

function colorazioneIcona() {
    var icona = document.getElementById("align-id");

    console.log(icona);

    

    icona.classList.toggle("fill-icon");
}


function colorazioneIcona2() {
    var icona = document.getElementById("align2");

    console.log(icona);

   
    icona.classList.toggle("fill-icon");
}

function colorazioneIcona3() {
    var icona = document.getElementById("align3");

    console.log(icona);

   
    icona.classList.toggle("fill-icon");
}





function prodottiDonna() {
    
    var scrollCardDonna = document.getElementById("scroll-card-donna");
    var scrollCard = document.getElementById("scroll-card");

    if (scrollCardDonna.className == "scroll-card-donna") {

        scrollCardDonna.className = "visibile";
        
        scrollCard.className = "none";
        
        
    } 



}

function prodottiUomo() {
    var scrollCardDonna = document.getElementById("scroll-card-donna");
    var scrollCard = document.getElementById("scroll-card");

    if (scrollCard.className == "none") {

        scrollCardDonna.className = "scroll-card-donna";
        
        scrollCard.className = "scroll-card";
        
        
    }

}

function prodottiKids() {
    var scrollCardKids = document.getElementById("scroll-card-kids");
    var scrollCardDonna = document.getElementById("scroll-card-donna");

    if (scrollCardKids.className == "scroll-card-kids") {

        scrollCardKids.className = "scrollCardKids";
        
        scrollCardDonna.className = "scroll-card-donna";
        
        
    }

}


var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}


var expanded = false;

function showCheckboxes2() {
  var checkboxes = document.getElementById("checkboxes2");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var expanded = false;

function showCheckboxes3() {
  var checkboxes = document.getElementById("checkboxes3");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var expanded = false;

function showCheckboxes4() {
  var checkboxes = document.getElementById("checkboxes4");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var expanded = false;

function showCheckboxes5() {
  var checkboxes = document.getElementById("checkboxes5");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var expanded = false;

function showCheckboxes6() {
  var checkboxes = document.getElementById("checkboxes6");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}


var expanded = false;

function showCheckboxes7() {
  var checkboxes = document.getElementById("checkboxes7");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var expanded = false;

function showCheckboxes8() {
  var checkboxes = document.getElementById("checkboxes8");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var expanded = false;

function showCheckboxes9() {
  var checkboxes = document.getElementById("checkboxes9");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}

var expanded = false;

function showCheckboxes10() {
  var checkboxes = document.getElementById("checkboxes10");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}


